﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace SimpleCalculator
{
    public static class Calculator
    {
        public static int Add(string insertedString)
        {
            if (string.IsNullOrEmpty(insertedString))
            {
                return 0;
            }

            string[] stringNumbers = insertedString.Split('\n', ',');

            int testNumber;
            string[] newStringDelimiters = null;
            if (!int.TryParse(stringNumbers[0], out testNumber))
            {
                newStringDelimiters = stringNumbers[0].Split(';');

                for (int i = 0; i < newStringDelimiters.Length; i++)
                {
                    newStringDelimiters[i] = RemoveWhitespaceAndBrackets(newStringDelimiters[i]);
                }
            }

            if (!(newStringDelimiters == null))
            {
                string[] delimiters = new string[newStringDelimiters.Length + 2];

                delimiters[0] = ",";
                delimiters[1] = "\n";

                for (int i = 2; i < delimiters.Length; i++)
                {
                    delimiters[i] = newStringDelimiters[i - 2];
                }

                stringNumbers = insertedString.Split(delimiters, StringSplitOptions.None);
            }

            int[] numbers = new int[stringNumbers.Length];

            for (int i = 0; i < stringNumbers.Length; i++)
            {
                int number;
                if (int.TryParse(stringNumbers[i], out number))
                {
                    if (number < 0)
                    {
                        throw new ArgumentException();
                    }

                    if (number > 1000)
                    {
                        numbers[i] = 0;
                        continue;
                    }

                    numbers[i] = number;
                }
                else
                {
                    numbers[i] = 0;
                }
            }

            int sum = 0;
            for (int i = 0; i < numbers.Length; i++)
            {
                sum += numbers[i];
            }

            return sum;
        }

        public static string RemoveWhitespaceAndBrackets(string input)
        {
            return new string(input.ToCharArray()
                .Where(c => (!(Char.IsWhiteSpace(c) || c == '[' || c == ']')))
                .ToArray());
        }
    }
}
