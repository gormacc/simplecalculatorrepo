using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleCalculator;
using System;

namespace UnitTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void InsertedStringIsEmpty()
        {
            int result = Calculator.Add("");

            Assert.AreEqual(result, 0);
        }

        [TestMethod]
        public void SingleNumber1()
        {
            int result = Calculator.Add("4");

            Assert.AreEqual(result, 4);
        }

        [TestMethod]
        public void SingleNumber2()
        {
            int result = Calculator.Add("13");

            Assert.AreEqual(result, 13);
        }

        [TestMethod]
        public void SingleNumber3()
        {
            int result = Calculator.Add("55");

            Assert.AreEqual(result, 55);
        }

        [TestMethod]
        public void TwoNumbers1Comma()
        {
            int result = Calculator.Add("3,4");

            Assert.AreEqual(result, 7);
        }

        [TestMethod]
        public void TwoNumbers2Comma()
        {
            int result = Calculator.Add(" 13 , 45 ");

            Assert.AreEqual(result, 58);
        }

        [TestMethod]
        public void TwoNumbers1NewLine()
        {
            int result = Calculator.Add("3\n4");

            Assert.AreEqual(result, 7);
        }

        [TestMethod]
        public void TwoNumbers2NewLine()
        {
            int result = Calculator.Add("   13    \n    45   ");

            Assert.AreEqual(result, 58);
        }

        [TestMethod]
        public void ThreeNumbers1Comma()
        {
            int result = Calculator.Add("3,4,5");

            Assert.AreEqual(result, 12);
        }

        [TestMethod]
        public void ThreeNumbers2Comma()
        {
            int result = Calculator.Add("    3   ,    4    ,    5    ");

            Assert.AreEqual(result, 12);
        }

        [TestMethod]
        public void ThreeNumbers1NewLine()
        {
            int result = Calculator.Add("3\n4\n5");

            Assert.AreEqual(result, 12);
        }

        [TestMethod]
        public void ThreeNumbers2NewLine()
        {
            int result = Calculator.Add("3  \n  4    \n   5");

            Assert.AreEqual(result, 12);
        }

        [TestMethod]
        public void ThreeNumbers1Combined()
        {
            int result = Calculator.Add("3  ,  4    \n   5");

            Assert.AreEqual(result, 12);
        }

        [TestMethod]
        public void ThreeNumbers2Combined()
        {
            int result = Calculator.Add("3  \n  4    ,   5");

            Assert.AreEqual(result, 12);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void NegativeNumber1()
        {
            int result = Calculator.Add("3,-4,5");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void NegativeNumber2()
        {
            int result = Calculator.Add("-3,4,5");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void NegativeNumber3()
        {
            int result = Calculator.Add("3,4,-5");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void NegativeNumber4()
        {
            int result = Calculator.Add("-3,-4,-5");
        }

        [TestMethod]
        public void GreaterThanOneThousand1()
        {
            int result = Calculator.Add("1234,4,5");

            Assert.AreEqual(result, 9);
        }

        [TestMethod]
        public void GreaterThanOneThousand2()
        {
            int result = Calculator.Add("3,42312321,5");

            Assert.AreEqual(result, 8);
        }

        [TestMethod]
        public void GreaterThanOneThousand3()
        {
            int result = Calculator.Add("3,4,5123123");

            Assert.AreEqual(result, 7);
        }

        [TestMethod]
        public void SingleCharDelimiter1()
        {
            int result = Calculator.Add(" # \n  3,4,5");

            Assert.AreEqual(result, 12);
        }

        [TestMethod]
        public void SingleCharDelimiter2()
        {
            int result = Calculator.Add(" # \n  3#4,5");

            Assert.AreEqual(result, 12);
        }

        [TestMethod]
        public void SingleCharDelimiter3()
        {
            int result = Calculator.Add(" ? \n  3,4?5");

            Assert.AreEqual(result, 12);
        }

        [TestMethod]
        public void MultipleCharDelimiter1()
        {
            int result = Calculator.Add(" [###] \n  3,4,5");

            Assert.AreEqual(result, 12);
        }

        [TestMethod]
        public void MultipleCharDelimiter2()
        {
            int result = Calculator.Add(" [###] \n  3###4,5");

            Assert.AreEqual(result, 12);
        }

        [TestMethod]
        public void MultipleCharDelimiter3()
        {
            int result = Calculator.Add(" [??] \n  3,4??5");

            Assert.AreEqual(result, 12);
        }

        [TestMethod]
        public void MultipleCharDelimiter4()
        {
            int result = Calculator.Add("[ ### ; ? ] \n  3,4,5");

            Assert.AreEqual(result, 12);
        }

        [TestMethod]
        public void MultipleCharDelimiter5()
        {
            int result = Calculator.Add(" [ ### ; ? ] \n  3###4?5");

            Assert.AreEqual(result, 12);
        }

        [TestMethod]
        public void MultipleCharDelimiter6()
        {
            int result = Calculator.Add(" [ ### ; ? ; ?? ; @! ] \n  3@!4??5");

            Assert.AreEqual(result, 12);
        }
    }
}
